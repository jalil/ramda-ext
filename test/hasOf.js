// @flow

require('jasmine-check').install();

import { hasOf } from '../src/hasOf';

function SomeClass () {
    this.someProp = 1;
    this.someMethod = () => {}
}

describe('hasOf', () => {
    const someObject = {
        aString: '',
        emptyString: '',
        typeString: String,
        aNumber: 42,
        anObject: {},
        anArray: [],
        anInstance: new SomeClass(),
    };

    it('returns true for props with correct type', () => {
        expect(hasOf(String, 'aString', someObject)).toEqual(true);
        expect(hasOf(String, 'emptyString', someObject)).toEqual(true);
        expect(hasOf(Number, 'aNumber', someObject)).toEqual(true);
        expect(hasOf(Object, 'anObject', someObject)).toEqual(true);
        expect(hasOf(SomeClass, 'anInstance', someObject)).toEqual(true);
    });

    it('returns false if prop contains type', () => {
        expect(hasOf(String, 'typeString', someObject)).toEqual(false);
    });

    it('returns false for undefined props', () => {
        expect(hasOf(String, 'nonExistent', someObject)).toEqual(false);
    });

    it('returns false for incorrect type', () => {
        expect(hasOf(Number, 'aString', someObject)).toEqual(false);
        expect(hasOf(Array, 'emptyString', someObject)).toEqual(false);
        expect(hasOf(String, 'aNumber', someObject)).toEqual(false);
    });
});
