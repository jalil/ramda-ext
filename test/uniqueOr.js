require ('jasmine-check').install();

import { uniqueOr } from "../src/uniqueOr";

describe('hasOneElement', () => {
    it('returns default value for an empty array', () => {
        expect(uniqueOr('default', [])).toEqual('default');
        expect(uniqueOr('other', [])).toEqual('other');
    });
    check.it('returns first value for an array with 1 element',
        gen.array(gen.any, { size: 1 }),
        arr => {
            expect(uniqueOr('default', arr)).toEqual(arr[0]);
        }
    );
    check.it('returns false for an array with 2 or more elements',
        gen.array(gen.any, { minSize: 2 }),
        arr => {
            expect(uniqueOr('default', arr)).toEqual('default');
            expect(uniqueOr('oth', arr)).toEqual('oth');
        }
    );
});
