import { hasValue } from "../src/hasValue";

require('jasmine-check').install();

describe('hasValue', () => {
    it('return false for non present key', () => {
        expect(hasValue('k', {}));
    });
});