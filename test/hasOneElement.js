require ('jasmine-check').install();

import { hasOneElement } from "../src/hasOneElement";

describe('hasOneElement', () => {
    it('returns false for an empty array', () => {
        expect(hasOneElement([])).toEqual(false);
    });
    check.it('returns true for an array with 1 element',
        gen.array(gen.any, { size: 1 }),
        arr => {
            expect(hasOneElement(arr)).toEqual(true);
        }
    );
    check.it('returns false for an array with 2 or more elements',
        gen.array(gen.any, { minSize: 2 }),
        arr => {
            expect(hasOneElement(arr)).toEqual(false);
        }
    );
});
