require('jasmine-check').install();

import { whenNotNil} from "../src/whenNotNil";

describe('whenNotNil', () => {
    let fn = jest.fn();

    beforeEach(() => { fn = jest.fn(); });

    it('does nothing when data is undefined', () => {
        whenNotNil(fn, undefined);
        expect(fn).not.toHaveBeenCalled();
    });

    it('does nothing when data is null', () => {
        whenNotNil(fn, null);
        expect(fn).not.toHaveBeenCalled();
    });

    it('applies function when data is empty string', () => {
        whenNotNil(fn, '');
        expect(fn).toHaveBeenCalledWith('');
    });

    it('applies function when data is false', () => {
        whenNotNil(fn, false);
        expect(fn).toHaveBeenCalledWith(false);
    });

    check.it('applies function when data is not nil', gen.any, data => {
        whenNotNil(fn, data);
        expect(fn).toHaveBeenCalledWith(data);
    });

    check.it('is currified', gen.any, data => {
        whenNotNil(fn)(data);
        expect(fn).toHaveBeenCalledWith(data);
    });
});
