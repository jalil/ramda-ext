require('jasmine-check').install();

import R from 'ramda';
import { whenNot} from "../src/whenNot";

describe('whenNotNil', () => {
    let fn = jest.fn();

    beforeEach(() => { fn = jest.fn(); });

    it.only('applies function when data is does NOT validate predicate', () => {
        whenNot(R.contains(1), fn, [0,2]);
        expect(fn).toHaveBeenCalled();
    });

    it('does nothing when data DOES validate predicate', () => {
        whenNot(R.isEmpty, fn, []);
        expect(fn).not.toHaveBeenCalled();
    });
});
