import R from 'ramda';

// Create new list from existing one and add two params : index & the list itsef
export const mapIndexed = R.addIndex(R.map);
