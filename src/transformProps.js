import R from 'ramda';

export const transformKeys = R.curry(
    (transform, obj) => R.reduce(
        (acc, key) => ({
            ...acc,
            [transform(key)]: obj[key],
        }),
        {},
        R.keys(obj)
    )
);
