import R from 'ramda';

export const mapWhen = R.curry(
    (predicat, morphism) => R.ifElse(
        predicat,
        R.map(morphism),
        morphism,
    )
);
