/* eslint-disable no-param-reassign */
import {
    hasValue, hasOf, isNotNil, whenNotNil, renameKeys, rejectNil, rejectN, limitTo, updateAt,
    lensDo, filterWithKeys, renameKey, mapIndexed, mergeDeep, mergeDeepWith, mergeDeepOverwriteArrays,
    titleize, transformKeys, whenNot, uniqueOr, hasOneElement, mapWhen,
} from './';

export default R => {
    if (!R.isExtended) {
        R.filterWithKeys = filterWithKeys;
        R.hasOf = hasOf;
        R.hasOneElement = hasOneElement;
        R.hasValue = hasValue;
        R.isNotNil = isNotNil;
        R.lensDo = lensDo;
        R.mapIndexed = mapIndexed;
        R.mapWhen = mapWhen;
        R.mergeDeep = mergeDeep;
        R.mergeDeepWith = mergeDeepWith;
        R.mergeDeepOverwriteArrays = mergeDeepOverwriteArrays;
        R.limitTo = limitTo;
        R.rejectN = rejectN;
        R.rejectNil = rejectNil;
        R.renameKey = renameKey;
        R.renameKeys = renameKeys;
        R.titleize = titleize;
        R.transformKeys = transformKeys;
        R.updateAt = updateAt;
        R.uniqueOr = uniqueOr;
        R.whenNot = whenNot;
        R.whenNotNil = whenNotNil;

        R.isExtended = true;
    }
};
