import R from 'ramda';
import { renameProps } from './renameProps';

export const renameProp = R.curry(
    (oldName, newName, obj) => renameProps({ [oldName]: newName }, obj)
);
