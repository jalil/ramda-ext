import R from 'ramda';

export const rejectN = R.curry(
    (numberToReject, predicate, array) => (
        R.reduce(
            (acc, item) => {
                const shouldReject = predicate(item) && acc.numberToReject;
                return {
                    n: shouldReject ? acc.numberToReject - 1 : acc.numberToReject,
                    t: shouldReject ? acc.array : [...acc.array, item],
                };
            },
            { numberToReject, array: [] },
            array
        )
            .array
    )
);
