// @flow

import R from 'ramda';

export const titleize = (text: string): string => R.pipe(
    R.over(
        R.lensIndex(0),
        R.toUpper
    ),
    R.join(''),
)(text);
