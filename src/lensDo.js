import R from 'ramda';

export const lensDo = R.curry((f, lens1, lens2, data) => (
    f(R.view(lens1, data), R.view(lens2, data))
));
