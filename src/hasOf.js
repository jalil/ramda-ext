// @flow

import R from 'ramda';

type hasOfDef = <T>(Function, T, {[T]:mixed}) => boolean;

export const hasOf:hasOfDef = R.curry(
    (type, prop, data) =>
        R.compose(R.is(type), R.prop(prop))(data)
);
