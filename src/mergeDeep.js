import R from 'ramda';

export const mergeDeepWith = concatArrays => R.curry(
    (first, second) => {
        if (R.is(Array, first) && R.is(Array, second))
            return concatArrays ? R.uniq(R.concat(first, second)) : second;
        if (R.is(Object, first) && R.is(Object, second))
            return R.mergeWith(mergeDeepWith(concatArrays), first, second);
        return second;
    }
);

export const mergeDeep = mergeDeepWith(true);

export const mergeDeepOverwriteArrays = mergeDeepWith(false);
