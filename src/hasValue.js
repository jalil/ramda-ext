import R from 'ramda';

type hasValueDef = <T>(T, {[T]: mixed}) => boolean;

export const hasValue:hasValueDef = R.curry(
    (prop, data) => R.compose(R.not, R.either(R.isEmpty, R.isNil), R.prop(prop))(data)
);
