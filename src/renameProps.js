import R from 'ramda';

export const renameProps = R.curry(
    (keysMap, obj) => (
        R.reduce((acc, key) => ({
            ...acc,
            [keysMap[key] || key]: obj[key],
        }), {}, R.keys(obj))
    )
);
