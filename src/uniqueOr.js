/* eslint-disable import/prefer-default-export */
import R from 'ramda';
import { hasOneElement } from './';

export const uniqueOr = R.curry(
    (defaultValue, array) =>
        R.ifElse(
            hasOneElement,
            R.head,
            R.always(defaultValue)
        )(array)
);
