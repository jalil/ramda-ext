import R from 'ramda';

export const whenNotNil = R.when(R.complement(R.isNil));
