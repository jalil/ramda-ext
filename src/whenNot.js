/* eslint-disable import/prefer-default-export */
import R from 'ramda';

export const whenNot = R.curry(
    (predicate, action, data) => R.when(R.complement(predicate), action, data)
);
