import R from 'ramda';

export const isNotNil = R.compose(R.not, R.isNil);
