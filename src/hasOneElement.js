/* eslint-disable import/prefer-default-export */
import R from 'ramda';

type hasOneElementDef = Array => boolean;

export const hasOneElement:hasOneElementDef = R.compose(R.equals(1), R.length);
