import R from 'ramda';

export const rejectNil = R.reject(R.isNil);
